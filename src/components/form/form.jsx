import React, { useState } from 'react'
import './form.scss'
import PhotoCameraIcon from '@mui/icons-material/PhotoCamera';
import { getStorage, ref, uploadBytesResumable, getDownloadURL } from "firebase/storage";
import { useLocation } from 'react-router-dom';
import app from '../../firebase';
import { createProduct, updateProduct } from '../../redux/apiCalls';
import { useDispatch } from 'react-redux';

const Form = ({item, update}) => {
  let location = useLocation().pathname.split("/")[2];
  const setValue = update;
  const [Categories, setCategories] = useState([]);
  const [Product, setProduct] = useState({})
  const [Image, setImage] = useState("")
  const [File, setFile] = useState(null);
  const dispatch = useDispatch(); 

  const handleChange = (e) => {
    setProduct( a => {
        return {...a, [e.target.name]: e.target.value}
    })
  }
  const handleclick = (e) => {
      e.preventDefault();
      if(File){
        const storage = getStorage(app);
        const storageRef = ref(storage, File.name);
        const uploadTask = uploadBytesResumable(storageRef, File);
        uploadTask.on('state_changed', 
        (snapshot) => {
        // Observe state change events such as progress, pause, and resume
        // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
        const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        console.log('Upload is ' + progress + '% done');
        switch (snapshot.state) {
        case 'paused':
            console.log('Upload is paused');
            break;
        case 'running':
            console.log('Upload is running');
            break;
        default:
        }
        }, 
        (error) => {
        // Handle unsuccessful uploads
        }, 
        () => {
        // Handle successful uploads on complete
        // For instance, get the download URL: https://firebasestorage.googleapis.com/...
        getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
                setImage(downloadURL);
                if(location === "New"){
                    const product = {...Product, img:downloadURL, categories:Categories };
                    createProduct(product, dispatch);
                    setValue(value => value++);
                }else{
                    const product = {...Product, _id:location, img:downloadURL, categories:Categories };
                    updateProduct(product,dispatch);
                    setValue(value => value++);
                }       
        });
        }
        );
        }else{
            if(location === "New"){
                const product = {...Product, categories:Categories };
                createProduct(product, dispatch);
                setValue(value => value++);
            }else{
                const product = {...Product,_id:location, categories:Categories };
                updateProduct(product,dispatch);
                setValue(value => value++);
            }      
        }
  }

  return (
    <div className='Form-widget'>
        <div className="form-left">
            <h2 className="form-title">{location === "New" || "new" ? "Add Products" : "Edit Products"}</h2>
            <div className="form-image">
                <div className="img-form-con">
                    <form className="upload-icon-container">
                        <label htmlFor="file"><PhotoCameraIcon className='form-icon'/></label>
                        <input type="file" id='file' accept="image/png, image/gif, image/jpeg" style={{display:"none"}} onChange={(e) => setFile(e.target.files[0])}/>
                    </form>
                    <img src={   File ? URL.createObjectURL(File) : 
                                 item ? item.img : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRT8-e9Jpr1AyNwkdf_iE_zQjknFwrn3kBbQQ&usqp=CAU" 
                             } alt="" className="img-form" />
                </div>
            </div>
        </div>
        <div className="form-right">
            <div className="form-title"></div>
            <form action="" className="form-main">
                <div className="form-input">
                    <label htmlFor="" className="form-label">Product :</label>
                    <input type="text" className="form-input-text" name='title' defaultValue={item ? item.title : ""} onChange={(e) => handleChange(e)}/>
                </div>
                <div className="form-input">
                    <label htmlFor="" className="form-label">Categories :</label>
                    <input type="text" className="form-input-text" name='categories' defaultValue={item ? item.categories : ""} onChange={(e) => setCategories(e.target.value.split(","))} />
                </div>
                <div className="form-input">
                    <label htmlFor="" className="form-label">Harga :</label>
                    <input type="text" className="form-input-text" name='price' defaultValue={item ? item.price : ""} onChange={(e) => handleChange(e)} />
                </div>
                <div className="form-input">
                    <label htmlFor="" className="form-label">description :</label>
                    <textarea className="form-input-text" name='description' defaultValue={item ? item.description : ""} onChange={(e) => handleChange(e)} />
                </div>
                <div className="form-input">
                    <label htmlFor="" className="form-label">Merk :</label>
                    <input type="text" className="form-input-text" name='merk' defaultValue={item ? item.merk : ""} onChange={(e) => handleChange(e)}/>
                </div>
                <div className="form-input">
                    <label htmlFor="" className="form-label">Stock</label>
                    <select name='inStock' className="form-input-select" onChange={(e) => handleChange(e)}>
                        <option value={true}  disabled selected={ item ? false : true}>Pilih ketersediaan stock</option>
                        <option value={true}  selected={ item ? item.inStock : false}>Tersedia</option>
                        <option value={false} selected={ item ? !item.inStock : false}>Habis</option>
                    </select>
                </div>
                <button onClick={(e) => handleclick(e)}>Send</button>
            </form>
        </div>
    </div>
  )
}

export default Form