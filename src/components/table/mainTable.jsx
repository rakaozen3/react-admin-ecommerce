import React from 'react'
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

const MainTable = () => {
    const rows = [{
        productId : 4325124131,
        productName : "playstation 5",
        img : 'testingg' ,
        customer : 'Rama perdana Bimantara',
        date : '24 march',
        amount : 400000,
        status : 'isApproved'
    },
    {
        productId : 4325124131,
        productName : "playstation 5",
        img : 'testingg' ,
        customer : 'Rama perdana Bimantara',
        date : '24 march',
        amount : 400000,
        status : 'isApproved'
    },
    {
        productId : 4325124131,
        productName : "playstation 5",
        img : 'testingg' ,
        customer : 'Rama perdana Bimantara',
        date : '24 march',
        amount : 400000,
        status : 'isApproved'
    },
    {
        productId : 4325124131,
        productName : "playstation 5",
        img : 'testingg' ,
        customer : 'Rama perdana Bimantara',
        date : '24 march',
        amount : 400000,
        status : 'isApproved'
    },
    {
        productId : 4325124131,
        productName : "playstation 5",
        img : 'testingg' ,
        customer : 'Rama perdana Bimantara',
        date : '24 march',
        amount : 400000,
        status : 'isApproved'
    }];
  return (
    <div className='main-table'>
        <TableContainer component={Paper}>
      <Table sx={{ minWidth: 1200 }} aria-label="simple table">
        <TableHead >
          <TableRow>
            <TableCell align="right">Product Id</TableCell>
            <TableCell align="right">Product Name</TableCell>
            <TableCell align="right">Images</TableCell>
            <TableCell align="right">Customer</TableCell>
            <TableCell align="right">Date</TableCell>
            <TableCell align="right">Amount</TableCell>
            <TableCell align="right">Status</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow
              key={row.productId}
            >
              <TableCell align="right">{row.productId}</TableCell>
              <TableCell align="right">{row.productName}</TableCell>
              <TableCell align="right">{row.img}</TableCell>
              <TableCell align="right">{row.customer}</TableCell>
              <TableCell align="right">{row.date}</TableCell>
              <TableCell align="right">{row.amount}</TableCell>
              <TableCell align="right">{row.status}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
    </div>
  )
}

export default MainTable