import React from 'react'
import './widget.scss'
import { CreditCard, KeyboardArrowUp, LocalOffer, LocalShipping, Person } from '@mui/icons-material'

const Widget = ({type}) => {
  let data;
  switch(type){
    case "users" :
      data={
        title: "Users",
        isMoney : false,
        HighlightText : 240,
        link : '/users',
        linkText : 'See All Users',
        Icon : <Person className='icon' style={{backgroundColor : "green"}} /> 
      }
      break;
    case "products" :
      data = {
        title: 'Products',
        isMoney : true,
        HighlightText : 5000000,
        link : '/products',
        linkText : 'See All Products',
        Icon : <LocalOffer className='icon' style={{backgroundColor : "blue"}}/> 
      }
      break;
      case "orders" :
        data = {
          title: 'Orders',
          isMoney : false,
          HighlightText : 5000000,
          link : '/products',
          linkText : 'See All Orders',
          Icon : <CreditCard className='icon' style={{backgroundColor : "crimson"}}/> 
        }
        break;
        case "deliveries" :
          data = {
            title: 'Deliveries',
            isMoney : false,
            HighlightText : 5000000,
            link : '/products',
            linkText : 'See All Deliveries',
            Icon : <LocalShipping className='icon' style={{backgroundColor : "purple"}}  /> 
          }
          break;
      default:
      break; 
  }
  return (
    <div className='widget'>
        <div className="left">
            <span className="title">{data.title}</span>
            <span className="change-counter">{data.isMoney && 'IDR.'}{data.HighlightText}</span>
            <span className="details">{data.linkText}</span>
        </div>
        <div className="right">
          <div className="percentage">
              <KeyboardArrowUp className='perc' />
              20%
          </div>
          <div className="icon-con">
              {data.Icon}
          </div>
        </div>
    </div>
  )
}

export default Widget