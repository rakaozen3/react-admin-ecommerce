import { MoreVert } from '@mui/icons-material'
import React from 'react'
import { CircularProgressbar } from 'react-circular-progressbar';
import "react-circular-progressbar/dist/styles.css"
import './featured.scss'

const Featured = () => {
  return (
    <div className='featured'>
        <div className="top">
            <h1 className="title">Total revenue</h1>
            <MoreVert  fontSize="small"/>
        </div>
        <div className="bottom">
            <div className="featuredchart">
                <CircularProgressbar value={70} text={"70%"} strokeWidth={3}/>
            </div>
            <p className="title">Total sales made today.</p>
            <p className="amount">$4200</p>
            <div className="desc">Previous transaction processing. Last payments might not included.</div>
            <div className="target">
                <p className="title">TARGET</p>
                <p className="amount">$10000</p>
            </div>
        </div>
    </div>
  )
}

export default Featured