import { ChatBubble, ChatBubbleOutline, ChatBubbleOutlined, DarkModeOutlined, FullscreenExitOutlined, Language, ListOutlined, NotificationsOutlined } from '@mui/icons-material'
import React from 'react'
import Lain from '../../assets/lain.jpg'
import './navbar.scss'

const Navbar = () => {
  return (
    <div className='Navbar'>
        <div className="wrapper">
            <div className="left">
                <input type="text" className="search" />
                <button className="search-button">Search</button>
            </div>
            <div className="right">
                <div className="item">
                    <Language /> 
                </div>
                <div className="item">
                    <NotificationsOutlined />
                    <div className="counter">1</div>
                </div>
                <div className="item">
                    <ChatBubbleOutline /> 
                    <div className="counter">2</div>
                </div>
                <div className="item">
                    <ListOutlined /> 
                </div>
                <div className="item">
                    <div className="profile-pic">
                        <img src={Lain} alt="" className="pic" />
                    </div>
                </div>
            </div>
        </div>
    </div>
  )
}

export default Navbar