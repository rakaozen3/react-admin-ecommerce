import React, { useEffect } from 'react'
import './Datatable.scss'
import { DataGrid } from '@mui/x-data-grid';
import { Link } from 'react-router-dom';
import { userscolumns, productscolumns, orderscolumns } from '../DataColumn';



const Datatable = ({Location, item}) => {
  let location = Location;
  console.log(location);
  
  const DataItem = item;

  return (
    <div className='datatable'>
        <DataGrid
          rows={DataItem}
          columns={ location == 'users' ? userscolumns : location == 'products' ? productscolumns : orderscolumns}
          getRowId={row => row._id}
          pageSize={10}
          rowsPerPageOptions={[10]}
          checkboxSelection
        />
    </div>
  )
}

export default Datatable