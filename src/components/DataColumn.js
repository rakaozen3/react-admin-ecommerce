import { useEffect } from 'react';
import { Link } from 'react-router-dom';



export const userscolumns = [
    { field: '_id', headerName: 'ID', width: 130 },
    { field: 'userName', headerName: 'User', width: 230,
            renderCell: (params) => {
              return(
                    <div className="cell-img-container">
                      <img src={params.row.image} alt="" className="cell-img" />
                      {params.row.userName}
                    </div>
                    
              )
            }
    },
    { field: 'email', headerName: 'Email', width: 230 },
    {
      field: 'status',
      headerName: 'Status',
      width: 200,
    },
    {
      field: 'Action',
      headerName: 'Action',
      sortable: false,
      width: 300,
      renderCell: (params)=> {
        return(
          <div className="action-button">
              <div className="action-button-con">
                <Link to={"/users/" + params.row._id} >
                  <button className="action-edit">
                    Edit
                  </button>
                </Link>
              </div>
              <div className="action-button-con">
                  <button className="action-delete">
                    Delete
                  </button>
              </div>
          </div>
        )
      }
    },
  ];

  export const productscolumns = [
    { field: '_id', headerName: 'Product ID', width: 230 },
    { field: 'title', headerName: 'Product Name', width: 230,
            renderCell: (params) => {
              return(
                    <div className="cell-img-container">
                      <img src={params.row.img} alt="" className="cell-img" />
                      {params.row.title}
                    </div>
                    
              )
            }
    },
    { field: 'categories', headerName: 'Category', width: 200 },
    { field: 'merk', headerName: 'Merk', width: 200 },
    { field: 'price', headerName: 'Price', width: 200 },
    {
      field: 'inStock',
      headerName: 'Status',
      width: 200,
    },
    {
      field: 'Action',
      headerName: 'Action',
      sortable: false,
      width: 230,
      renderCell: (params)=> {
        return(
          <div className="action-button">
              <div className="action-button-con">
                <Link to={"/products/" + params.row._id} >
                  <button className="action-edit">
                    Edit
                  </button>
                </Link>
              </div>
              <div className="action-button-con">
                  <button className="action-delete">
                    Delete
                  </button>
              </div>
          </div>
        )
      }
    },
  ];

  export const orderscolumns = [
    { field: '_id', headerName: 'Order ID', width: 230 },
    { field: 'userId', headerName: 'Users', width: 200 },
    { field: 'title', headerName: 'Customer Name', width: 200,
            renderCell: (params) => {
              return(
                    <div className="cell-img-container">
                      <img src={params.row.image} alt="" className="cell-img" />
                      {params.row.userName}
                    </div>
                    
              )
            }
    },
    { field: 'amount', headerName: 'Amount', width: 200 },
    { field: 'address', headerName: 'Address', width: 300 },
    { field: 'status', headerName: 'Status', width: 200 },
    {
      field: 'Action',
      headerName: 'Action',
      sortable: false,
      width: 230,
      renderCell: (params)=> {
        return(
          <div className="action-button">
              <div className="action-button-con">
                <Link to={"/products/" + params.row._id} >
                  <button className="action-edit">
                    Edit
                  </button>
                </Link>
              </div>
              <div className="action-button-con">
                  <button className="action-delete">
                    Delete
                  </button>
              </div>
          </div>
        )
      }
    },
  ];