import { NavLink } from 'react-router-dom';
import React from 'react';
import './sidebar.scss';
import { logoutSuccess } from '../../redux/userRedux';
import { CreditCard, Dashboard, LocalShipping, Logout, Person, Settings, ShoppingBag } from '@mui/icons-material'
import { useDispatch } from 'react-redux'

const Sidebar = () => {
  const dispatch = useDispatch();

  const handleLogout = () => {
    dispatch(logoutSuccess());
  }

  return (
    <div className='Sidebar'>
        <div className="top">
            <div className="logo">
                 <div className="logo-con">
                     <span className="logo">CAMPROSBY</span>
                 </div>
            </div>
        </div>
        <hr /> 
        <div className="bottom">
            <p className="title">Main</p>
            <NavLink
            to="/"
            >
                <div className="nav-lists">
                    <Dashboard className='icon' />
                    <span className="nav-text">Dashboard</span>
                </div>
            </NavLink>
            <p className="title">Lists</p>
            <NavLink
            to="/users"
            >
            <div className="nav-lists">
                <Person className='icon' />
                <span className="nav-text">Users</span>
            </div>
            </NavLink>
            <NavLink
            to="/products"
            >
            <div className="nav-lists">
                <ShoppingBag className='icon' />
                <span className="nav-text">Products</span>
            </div>
            </NavLink>
            <NavLink
            to="/orders"
            >
            <div className="nav-lists">
                <CreditCard className='icon' />
                <span className="nav-text">Orders</span>
            </div>
            </NavLink>
            {/* <NavLink
            to="/Delivery"
            >
            <div className="nav-lists">
                <LocalShipping className='icon' />
                <span className="nav-text">Delivery</span>
            </div>
            </NavLink> */}
            <p className="title">Users</p>
            <div className="nav-lists">
                <Settings className='icon' />
                <span className="nav-text">Settings</span>
            </div>
            <div className="nav-lists" onClick={handleLogout}>
                <Logout className='icon'/>
                <span className="nav-text">Log out</span>
            </div>
        </div>
    </div>
  )
}

export default Sidebar