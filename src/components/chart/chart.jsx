import React from 'react';
import { PureComponent } from 'react';
import { AreaChart, LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer, Area } from 'recharts';
import './chart.scss'

const data = [
  {
    name: 'January',
    uv: 4000,
  },
  {
    name: 'Feburary',
    uv: 3000,
  },
  {
    name: 'March',
    uv: 2000,
  },
  {
    name: 'April',
    uv: 2780,

  },
  {
    name: 'May',
    uv: 1890,
  },
  {
    name: 'June',
    uv: 2390,
  },
  {
    name: 'July',
    uv: 3490,
  },
];

const Chart = ({aspect, title}) => {
  return (
    <div className='chart'>
      <h3 className="chart-title">{title}</h3>
          <ResponsiveContainer width="100%" aspect={aspect}>
            <AreaChart
              width={500}
              height={300}
              data={data}
              margin={{
                top: 30,
                right: 30,
                left: 30,
                bottom: 10,
              }}
            >
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="name" />
              <YAxis />
              <Tooltip />
              {/* <Legend /> */}
              <Area type="monotone" dataKey="uv" stroke="#8884d8" fill="#6439ff" fillOpacity={0.5} />
              {/* <Line type="monotone" dataKey="uv" stroke="#82ca9d" /> */}
            </AreaChart>
          </ResponsiveContainer>
    </div>
  )
}

export default Chart