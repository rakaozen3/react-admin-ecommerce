import { publicRequest, userRequest } from "../requestMethod";
import { 
    getProductStart, 
    getProductSuccess, 
    getProductFailure, 
    deleteProductStart, 
    deleteProductSuccess, 
    deleteProductFailure,
    createProductStart, 
    createProductSuccess, 
    createProductFailure, 
    updateProductStart, 
    updateProductSuccess, 
    updateProductFailure} from "./productRedux";
import { loginFailure, loginStart, loginSuccess} from "./userRedux"

export const login = async (dispatch, user) => {
    dispatch(loginStart());
    try{
        const res = await publicRequest.post("/users/login", user);
        dispatch(loginSuccess(res.data));
    }catch(err){
        dispatch(loginFailure());
    }
};

export const getProduct = async (dispatch) => {
    dispatch(getProductStart());
    try{
        const res = await publicRequest.get("/products/");
        dispatch(getProductSuccess(res.data));
    }catch(err){
        dispatch(getProductFailure());
    }
};

export const deleteProduct = async (id, dispatch) => {
    dispatch(deleteProductStart());
    try{
        await userRequest.delete(`/products/${id}`);
        dispatch(deleteProductSuccess(id));
    }catch(err){
        dispatch(deleteProductFailure());
    }
};

export const updateProduct = async (product, dispatch) => {
    dispatch(updateProductStart());
    try{
        await userRequest.put(`/products/${product._id}`, product);
        dispatch(updateProductSuccess({product}));
    }catch(err){
        dispatch(updateProductFailure());
    }
};

export const createProduct = async (product, dispatch) => {
    dispatch(createProductStart());
    try{
        const res = await userRequest.post(`/products/create`, product);
        dispatch(createProductSuccess(res.data));
    }catch(err){
        dispatch(createProductFailure());
    }
};