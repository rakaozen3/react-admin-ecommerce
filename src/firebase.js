// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBAR1i-FjYecGGFOWfO3Nwx-fLXI122OwE",
  authDomain: "camprosbyfilemanager.firebaseapp.com",
  projectId: "camprosbyfilemanager",
  storageBucket: "camprosbyfilemanager.appspot.com",
  messagingSenderId: "139499403382",
  appId: "1:139499403382:web:56d97d85ae8774498f8757"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export default app; 