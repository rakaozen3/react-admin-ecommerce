import React from 'react'
import './Home.scss'
import Sidebar from '../../components/sidebar/sidebar'
import Navbar from '../../components/navbar/navbar'
import Widget from '../../components/widgets/widget'
import Featured from '../../components/featured/featured'
import Chart from '../../components/chart/chart'
import MainTable from '../../components/table/mainTable'

const Home = () => {
  return (
    <div className="home">
        <Sidebar /> 
        <div className="main-panel">
          <Navbar />
          <div className="Widgets">
            <Widget type="users" /> 
            <Widget type="products"  />
            <Widget type="orders"  />
            <Widget type="deliveries"  />
          </div>
          <div className="charts">
            <Featured />
            <Chart aspect={2/0.85} title="Last 6 Months Revenue"/> 
          </div>
          <div className="tablecontainer">
            <div className="table-title">Latest Transactions</div>
            <MainTable /> 
          </div>
        </div>
    </div>
  )
}

export default Home