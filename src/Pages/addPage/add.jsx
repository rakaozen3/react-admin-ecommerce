import React from 'react';
import './add.scss';
import Form from '../../components/form/form';
import Navbar from '../../components/navbar/navbar';
import Sidebar from '../../components/sidebar/sidebar';

const Add = () => {
  return (
    <div className='add-page'>
      <Sidebar />
      <div className="main-addpage">
        <Navbar /> 
        <div className="addpage-main-content">
          <Form title="Add User"/> 
        </div>
      </div>
    </div>
  )
}

export default Add