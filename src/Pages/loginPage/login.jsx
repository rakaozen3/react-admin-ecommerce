import React, { useState } from 'react'
import { useDispatch } from 'react-redux';
import { login } from '../../redux/apiCalls'

const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const dispatch = useDispatch();
  const handleClick = () => {
    login(dispatch, {username, password});
  }

  return (
    <div>
      <input type="text" name='username' placeholder='username' onChange={(e) => {setUsername(e.target.value)}}/>
      <input type="password" name="password" id="" placeholder='password' onChange={(e) => {setPassword(e.target.value)}} />
      <button className="login" onClick={handleClick}>Login</button>
    </div>
  )
}

export default Login