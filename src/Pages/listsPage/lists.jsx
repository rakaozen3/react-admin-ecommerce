import React, { useEffect, useState } from 'react';
import './lists.scss'
import Navbar from '../../components/navbar/navbar';
import Sidebar from '../../components/sidebar/sidebar';
import Datatable from '../../components/Datatable/Datatable';
import { useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getProduct } from '../../redux/apiCalls'
import { userRequest } from '../../requestMethod';

const Lists = () => {
  let location = useLocation().pathname.split("/")[1];
  const products = useSelector(state => state.product.products)
  const [User, setUser] = useState([]);
  const [Order, setOrder] = useState([]);
  const dispatch = useDispatch();
  console.log(products);
  
  useEffect(() => {
    getProduct(dispatch)
  }, [dispatch])

  useEffect(() => {
    const getUser = async () => {
        try{
          const res = await userRequest.get("/users/");
          setUser(res.data);
        } 
        catch(err) {
          console.log(err);
        }
    }
    getUser();
  },[]);

  useEffect(() => {
    const getOrder = async () => {
        try{
          const res = await userRequest.get("/order/");
          setOrder(res.data);
        } 
        catch(err) {
          console.log(err);
        }
    }
    getOrder();
  },[]);

  console.log(Order);



  

  return (
    <div className='lists'>
        <Sidebar />
        <div className="main-lists">
          <Navbar /> 
          <Datatable Location={location} item={location === "users" ? User : location === "products" ? products : Order}/> 
        </div>
    </div>
  )
}

export default Lists