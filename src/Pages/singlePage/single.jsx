import React, { useEffect, useState } from 'react';
import './Single.scss';
import Sidebar from '../../components/sidebar/sidebar';
import Navbar from '../../components/navbar/navbar';
import Chart from '../../components/chart/chart';
import Form from '../../components/form/form';
import { useLocation } from 'react-router-dom';
import { userRequest } from '../../requestMethod';

const Single = () => {
  const id = useLocation().pathname.split("/")[2];
  const [Edit, setEdit] = useState(false)
  const [product, setProduct] = useState({});
  const [Value, setValue] = useState(0);
  useEffect(() => {
    const getProducts = async () => {
        try{
          const res = await userRequest.get("/products/"+id)
          setProduct(res.data);
        } 
        catch(err) {
          console.log(err);
        }
    }

   getProducts();
  
  }, [id]);
  
  return (
    <div className='single-page'>
      <Sidebar /> 
      <div className="single-container">
        <Navbar />
        <div className="single-page">
            <div className="single-top">
                <div className="single-topleft">
                  <button className="single-edit-btn" onClick={()=> { setEdit(!Edit)}}>Edit</button>
                  <h1 className="single-title">Product Information</h1>
                  <div className="single-item">
                    <div className="single-item-img-con">
                        <div className="item-img">
                            <img src={product.img ? product.img : "https://lh3.googleusercontent.com/-2q5xOhApNK8/XvDMjLyg_BI/AAAAAAAAHvw/HJrA-s16Vo4BftnFvLZi73tRL5pVKbgFgCLcBGAsYHQ/s1600/1592839306156739-0.png"} alt="" className="img-details" />
                        </div>
                    </div>
                    <div className="single-item-details">
                      <h2 className="single-item-name">{product.title}</h2>
                      <div className="details-map">
                            <span className="item-key">Harga : </span>
                            <span className="item-value">{product.price}</span>
                      </div>
                      <div className="details-map">
                            <span className="item-key">Categories : </span>
                            <span className="item-value">{product.categories?.join(", ")}</span>
                      </div>
                      <div className="details-map">
                            <span className="item-key">Deskripsi : </span>
                            <span className="item-value">{product.description}</span>
                      </div>
                      <div className="details-map">
                            <span className="item-key">Harga : </span>
                            <span className="item-value">{product.price}</span>
                      </div>
                      <div className="details-map">
                            <span className="item-key">Merk : </span>
                            <span className="item-value">{product.merk}</span>
                      </div>
                      <div className="details-map">
                            <span className="item-key">Stock : </span>
                            <span className="item-value">{product.inStock ? "Tersedia" : "Habis"}</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="single-topright">
                  <Chart aspect={2/0.75} title="User spending of the last 6 months"/> 
                </div>
            </div>
            <div className="single-bottom">
              {Edit && <Form item={product} update={setValue}/> }
            </div>
        </div>
      </div>

    </div>
  )
}

export default Single