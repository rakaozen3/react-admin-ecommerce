import { render } from "react-dom";
import {
  BrowserRouter,
  Routes,
  Route,
  Navigate,
} from "react-router-dom";
import './App.css';
import Home from './Pages/homePage/Home';
import Single from "./Pages/singlePage/single";
import Lists from "./Pages/listsPage/lists";
import Add from "./Pages/addPage/add";
import Login from "./Pages/loginPage/login";
import ProctectedRoutes from "./ProctectedRoutes";
import { useSelector } from "react-redux";

function App() {
  const user = useSelector(state => state.user.currentUser);
  return (
    <div className="App">
      <BrowserRouter>
      <Routes>
        <Route path="login" element={ user ? <Navigate to={"/"} /> : <Login />  } />
        <Route path="/" element={<ProctectedRoutes />}>
          <Route index element={<Home />} />
          <Route path="users" >
            <Route index element={<Lists />} />
            <Route path=":userId" element={<Single />} />
            <Route path="New" element={<Add />} />
          </Route>
          <Route path="products">
            <Route index element={<Lists />} />
            <Route path=":productsId" element={<Single />} />
            <Route path="New" element={<Add />} />
          </Route>
          <Route path="orders">
            <Route index element={<Lists />} />
            <Route path=":ordersId" element={<Single />} />
          </Route>
          <Route path="Delivery">
            <Route index element={<Lists />} />
            <Route path=":deliveryId" element={<Single />} />
            <Route path="New" element={<Add />} />
          </Route>
        </Route>
      </Routes>
    </BrowserRouter>
    </div>
  );
}

export default App;
